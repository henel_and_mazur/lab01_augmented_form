from saport.simplex.model import Model
from saport.simplex.expressions.expression import Expression

def assignment_1():
    model = Model("Assignment 1")
    value1 = model.create_variable("value1")   # x1
    value2 = model.create_variable("value2")   # x2
    value3 = model.create_variable("value3")   # x3

    line1 = value1 + value2 + value3
    line2 = value1 + 2 * value2 + value3
    line3 = 2 * value2 + value3

    model.add_constraint(line1 <= 30)
    model.add_constraint(line2 >= 10)
    model.add_constraint(line3 <= 20)

    model.maximize(2 * value1 + value2 + 3 * value3)

    print("Before solving:")
    print(model)

    solution = model.solve()

    print("Solution: ")
    print(solution)

    return model

def assignment_2():
    model = Model("Assignment 2")
    P1 = model.create_variable("P1")  # ilosc P1
    P2 = model.create_variable("P2")  # ilosc P2
    P3 = model.create_variable("P3")  # ilosc P3
    P4 = model.create_variable("P4")  # ilosc P4

    S1 = 0.8 * P1 + 2.4 * P2 + 0.9 * P3 + 0.4 * P4
    S2 = 0.6 * P1 + 0.6 * P2 + 0.3 * P3 + 0.3 * P4

    model.add_constraint(S1 >= 1200)
    model.add_constraint(S2 >= 600)

    model.minimize(9.6 * P1 + 14.4 * P2 + 10.8 * P3 + 7.2 * P4)

    print("Before solving:")
    print(model)

    solution = model.solve()

    print("Solution: ")
    print(solution)

    return model

def assignment_3():
    model = Model("Assignment 3")
    s = model.create_variable("s") #steki
    z = model.create_variable("z") #ziemniaki

    weglowodany = 5 * s + 15 * z
    bialka = 20 * s + 5 * z
    tluszcze = 15 * s + 2 * z

    model.add_constraint(weglowodany >= 50)
    model.add_constraint(bialka >= 40)
    model.add_constraint(tluszcze <= 60)

    model.minimize(8 * s + 4 * z)

    print("Before solving:")
    print(model)

    solution = model.solve()

    print("Solution: ")
    print(solution)

    return model

def assignment_4():
    model = Model("Assignment 4")
    """
    numer sposobu       x1  x2  x3  x4  x5  x6  x7  x8  x9  x10 x11 x12 x13 x14 x15    
    ilość zwojów 105cm  1   1   0   0   0   0   1   1   0   0   0   0   0   0   0
    ilość zwojów 75cm   1   0   1   2   0   1   0   0   1   1   0   0   0   2   0
    ilość zwojów 35cm   0   2   3   1   5   0   0   1   1   2   1   2   3   0   4
    łączna dlugosc      180 175 180 185 175 75  105 140 110 145 35  70  105 150 140
    odpad               20  25  20  15  25  125 95  60  90  55  165 130 95  50  60
    
    """
    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")
    x4 = model.create_variable("x4")
    x5 = model.create_variable("x5")
    x6 = model.create_variable("x6")
    x7 = model.create_variable("x7")
    x8 = model.create_variable("x8")
    x9 = model.create_variable("x9")
    x10 = model.create_variable("10")
    x11 = model.create_variable("x11")
    x12 = model.create_variable("x12")
    x13 = model.create_variable("x13")
    x14 = model.create_variable("x14")
    x15 = model.create_variable("x15")

    cm105 = 1 * x1 + 1 * x2 + 0 * x3 + 0 * x4 + 0 * x5 + 0 * x6 + 1 * x7 + 1 * x8 + 0 * x9 + 0 * x10 + 0 * x11 + 0 * x12 + 0 * x13 + 0 * x14 + 0 * x15
    cm75 = 1 * x1 + 0 * x2 + 1 * x3 + 2 * x4 + 0 * x5 + 1 * x6 + 0 * x7 + 0 * x8 + 1 * x9 + 1 * x10 + 0 * x11 + 0 * x12 + 0 * x13 + 2 * x14 + 0 * x15
    cm35 = 0 * x1 + 2 * x2 + 3 * x3 + 1 * x4 + 5 * x5 + 0 * x6 + 0 * x7 + 1 * x8 + 1 * x9 + 2 * x10 + 1 * x11 + 2 * x12 + 3 * x13 + 0 * x14 + 4 * x15

    model.add_constraint(cm105 >= 150)
    model.add_constraint(cm75 >= 200)
    model.add_constraint(cm35 >= 150)

    model.minimize(20 * x1 + 25 * x2 + 20 * x3 + 15 * x4 + 25 * x5 + 125 * x6 + 95 * x7 + 60 * x8 + 90 * x9 + 55 * x10 + 165 * x11 + 130 * x12 + 95 * x13 + 50 * x14 + 60 * x15)

    print("Before solving:")
    print(model)

    solution = model.solve()

    print("Solution: ")
    print(solution)

    return model
